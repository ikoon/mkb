/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
      $('.user-not-logged-in #header-wrapper' ).clone().prependTo( '#header-wrapper' );
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        var objectSelect = $(".view-mode-full");
        var objectPosition = objectSelect.offset().top -650;
        if( scroll > objectPosition ) {
          $('body').addClass('fixed-header-now');
        } else {
          $('body').removeClass('fixed-header-now');
        }
      });
    }
  };

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.cookieSettings = {
    attach: function (context, settings) {
      $('span[data-action="cookie_settings"]', context).click(function() {
        $('.eu-cookie-withdraw-tab').click();
      });
      $(function(){
        $('#sliding-popup').css({bottom: - $('#sliding-popup').height()});
      });
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-slides.swiper-container').once('ifSlider').each(function (){
                var slides = new Swiper (this, {
                    autoplay: true,
                    loop: true,
                    effect: 'fade',
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                        clickable: true,
                    },
                    navigation: {
                      nextEl: '.swiper-button-next',
                      prevEl: '.swiper-button-prev',
                    },
                });
            });
        }
    };

  Drupal.behaviors.imagesLoaded = {
    attach: function (context, settings) {
      $('#lightgallery').lightGallery();

      $('.grid').imagesLoaded(function () {
        $('.grid').masonry({
          itemSelector: '.grid-item',
          percentPosition: false,
          horizontalOrder: true,
          gutter: 0,
          fitWidth: false
        });
      });
    }
  };

})(jQuery, Drupal);
